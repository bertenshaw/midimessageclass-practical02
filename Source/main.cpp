//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>

class MidiMessage
{
public:
    MidiMessage() //Constructor
    {
        number = velocity = 60;
        channel = 1;
    }
    ~MidiMessage() //Destructor
    {}
    void setNoteNumber (int value) //Mutator
    {
        if (value >= 0 && value <= 127)
        {
        number = value;
        }
    }
    void setVelocity(int value) //Mutator
    {
        if (value >= 0 && value <= 127)
        {
            velocity = value;
        }
    }
    void setMidiChannel(int value) //Mutator
    {
        if (value >= 0 && value <= 127)
        {
            channel = value;
        }
    }
    int getNoteNumber() const //Accessor
    {
        return number;
    }
    float getMidiNoteInHertz() const //Accessor
    {
        return 440 * pow(2, (number-69) / 12.0);
    }
    float getFloatVelocity() const // Accessor
    {
        return velocity / 127;
    }
    int getMidiChannel() const //Accessor
    {
        return channel;
    }
private:
    int number, velocity, channel;
};

int main (int argc, const char* argv[])
{
    MidiMessage MidiMessage1;
    int noteIn;
    
    std::cout << "Note Number:" << MidiMessage1.getNoteNumber() << "\nFrequency:" << MidiMessage1.getMidiNoteInHertz() << "\nPlease enter a new midi note number: ";
    
    std::cin >> noteIn;
    MidiMessage1.setNoteNumber(noteIn);
    
    std::cout << "Note Number:" << MidiMessage1.getNoteNumber() << "\nFrequency:" << MidiMessage1.getMidiNoteInHertz() << "\n";
    
    return 0;
}

